package com.ada.iwan.controller.admin;



import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ada.data.page.Order;
import com.ada.data.page.Page;
import com.ada.data.page.Pageable;
import com.ada.album.entity.Category;
import com.ada.album.service.CategoryService;
@Controller
public class CategoryAction {
	private static final Logger log = LoggerFactory.getLogger(CategoryAction.class);

	@RequestMapping("/admin/category/view_list")
	public String list(Pageable pageable, HttpServletRequest request, ModelMap model) {
	
		if (pageable==null) {
			pageable=new Pageable();
		}
		if (pageable.getOrders()==null||pageable.getOrders().size()==0) {
			pageable.getOrders().add(Order.desc("id"));
		}
		Page<Category> pagination = manager.findPage(pageable);
		model.addAttribute("list", pagination.getContent());
		model.addAttribute("page", pagination);
		return "admin/category/list";
	}

	@RequestMapping("/admin/category/view_add")
	public String add(ModelMap model) {
		return "admin/category/add";
	}

	@RequestMapping("/admin/category/view_edit")
	public String edit(Pageable pageable,String id, Integer pageNo, HttpServletRequest request, ModelMap model) {
		model.addAttribute("model", manager.findById(id));
		model.addAttribute("pageNo", pageNo);
		model.addAttribute("page", pageable);
		return "admin/category/edit";
	}

	@RequestMapping("/admin/category/model_save")
	public String save(Category bean, HttpServletRequest request, ModelMap model) {
		bean = manager.save(bean);
		log.info("save Teacher id={}", bean.getId());
		return "redirect:view_list.htm";
	}

	@RequestMapping("/admin/category/model_update")
	public String update(Pageable pageable, Category bean,HttpServletRequest request, ModelMap model) {
		bean = manager.update(bean);
		log.info("update Teacher id={}.", bean.getId());
		return "redirect:/admin/category/view_list.htm?pageNumber="+pageable.getPageNumber();
	}

	@RequestMapping("/admin/category/model_delete")
	public String delete(Pageable pageable, String id, HttpServletRequest request, ModelMap model) {
			 
				manager.deleteById(id);
			 
		return "redirect:/admin/category/view_list.htm?pageNumber="+pageable.getPageNumber();
	}
	@RequestMapping("/admin/category/model_deletes")
	public String deletes(Pageable pageable, String[] ids, HttpServletRequest request, ModelMap model) {
			 
				manager.deleteByIds(ids);
			 
		return "redirect:/admin/category/view_list.htm?pageNumber="+pageable.getPageNumber();
	}
	@Autowired
	private CategoryService manager;
}