package com.ada.iwan.service.album;

import java.util.List;

public class Wallpaper {

	private List<PhotoVo> wallpaper;

	public List<PhotoVo> getWallpaper() {
		return wallpaper;
	}

	public void setWallpaper(List<PhotoVo> wallpaper) {
		this.wallpaper = wallpaper;
	}
	
}
