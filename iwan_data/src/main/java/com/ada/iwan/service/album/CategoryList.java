package com.ada.iwan.service.album;

import java.util.List;

import com.ada.album.entity.Category;

public class CategoryList {

	private List<Category> category;

	public List<Category> getCategory() {
		return category;
	}

	public void setCategory(List<Category> category) {
		this.category = category;
	}
}
