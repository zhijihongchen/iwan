package com.tongna.shopdeals.domain;

import java.util.List;

public class CategoriesListBack extends Back{
	private List<Categories> categories;

	public List<Categories> getCategories() {
		return categories;
	}

	public void setCategories(List<Categories> categories) {
		this.categories = categories;
	}
}
