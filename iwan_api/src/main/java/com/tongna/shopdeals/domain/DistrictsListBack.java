package com.tongna.shopdeals.domain;

import java.util.List;

public class DistrictsListBack extends Back {
	private List<Districts> districts;

	public List<Districts> getDistricts() {
		return districts;
	}

	public void setDistricts(List<Districts> districts) {
		this.districts = districts;
	}

}
