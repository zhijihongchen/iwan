package com.tongna.shopdeals.domain;

import java.util.List;

public class ShopDealsListBack extends Back {

	private List<ShopDeals> deals;

	public List<ShopDeals> getDeals() {
		return deals;
	}

	public void setDeals(List<ShopDeals> deals) {
		this.deals = deals;
	}

}
