package com.tongna.shopdeals.domain;

public class ShopInfoBack extends Back {
	/**
	 * 构建商户对象
	 */
	private ShopInfo shop;

	public ShopInfo getShop() {
		return shop;
	}

	public void setShop(ShopInfo shop) {
		this.shop = shop;
	}

	
}
