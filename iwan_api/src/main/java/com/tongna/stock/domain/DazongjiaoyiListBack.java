package com.tongna.stock.domain;

import java.util.List;

public class DazongjiaoyiListBack extends Back {
	/**
	 * 创建交易信息对象
	 */
	
	private List<Dazongjiaoyi> rows;

	public List<Dazongjiaoyi> getRows() {
		return rows;
	}

	public void setRows(List<Dazongjiaoyi> rows) {
		this.rows = rows;
	}
}
